package com.playground

import com.playground.generated.CalculatorGrpcKt
import com.playground.generated.CalculatorRequest
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import java.io.Closeable
import java.util.concurrent.TimeUnit
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import mu.KotlinLogging

class BidiConsumer(private val channel: ManagedChannel) : Closeable {
    private val logger = KotlinLogging.logger {}
    private val stub: CalculatorGrpcKt.CalculatorCoroutineStub = CalculatorGrpcKt.CalculatorCoroutineStub(channel)

    suspend fun getContinuousSum(numberList: List<Pair<Int, Int>>) {
        val responses = stub.getContinuousSum( flow {
            numberList.forEach {
                val request = CalculatorRequest
                    .newBuilder()
                    .setNumber1(it.first)
                    .setNumber2(it.second)
                    .build()
                emit(request)
                delay(3000)
                logger.info("BidiConsumer: emitted 1 pair (${it.first}, ${it.second}) to server")
            }
        })
        responses.collect { response ->
            logger.info("BidiConsumer: current total ${response.result}")
        }
        logger.info("BidiConsumer: ending summation")
    }

    override fun close() {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS)
    }
}

suspend fun main() {
    val port = 50051
    val channel = ManagedChannelBuilder.forAddress("localhost", port).usePlaintext().build()
    val client = BidiConsumer(channel)
    client.getContinuousSum(
        listOf(Pair(1, 5), Pair(9, 3), Pair(2, 8), Pair(3, 7), Pair(5, 4))
    )
}