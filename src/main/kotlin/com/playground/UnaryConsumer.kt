package com.playground

import com.playground.generated.CalculatorGrpcKt
import com.playground.generated.CalculatorRequest
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import java.io.Closeable
import java.util.concurrent.TimeUnit
import mu.KotlinLogging

class UnaryConsumer(private val channel: ManagedChannel) : Closeable {
    private val logger = KotlinLogging.logger {}
    private val stub: CalculatorGrpcKt.CalculatorCoroutineStub = CalculatorGrpcKt.CalculatorCoroutineStub(channel)

    suspend fun sumOnce(num1: Int, num2: Int) {
        val request = CalculatorRequest
            .newBuilder()
            .setNumber1(num1)
            .setNumber2(num2)
            .build()
        val response = stub.sumOnce(request)
        logger.info("UnaryConsumer: result ${response.result}")
    }

    override fun close() {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS)
    }
}

suspend fun main() {
    val port = 50051
    val channel = ManagedChannelBuilder.forAddress("localhost", port).usePlaintext().build()
    val client = UnaryConsumer(channel)
    client.sumOnce(1, 2)
}