package com.playground

import com.playground.generated.CalculatorGrpcKt
import com.playground.generated.CalculatorResponse
import com.playground.generated.CalculatorRequest
import io.grpc.Server
import io.grpc.ServerBuilder
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import mu.KotlinLogging

class ServerStreamingProducer(private val port: Int) {
    private val logger = KotlinLogging.logger {}
    val server: Server = ServerBuilder
        .forPort(port)
        .addService(ServerStreamingCalculatorService())
        .build()

    fun start() {
        server.start()
        logger.info("Server started, listening on $port")
        Runtime.getRuntime().addShutdownHook(
            Thread {
                logger.info("*** shutting down gRPC server since JVM is shutting down")
                this@ServerStreamingProducer.stop()
                logger.info("*** server shut down")
            }
        )
    }

    private fun stop() {
        server.shutdown()
    }

    fun blockUntilShutdown() {
        server.awaitTermination()
    }

    private class ServerStreamingCalculatorService : CalculatorGrpcKt.CalculatorCoroutineImplBase() {
        private val logger = KotlinLogging.logger {}
        // full result already in array list and return it as flow (will not be able to delay the flow)
//        override fun deriveSubsequent(request: CalculatorRequest): Flow<CalculatorResponse> {
//            val diff = request.number2 - request.number1
//            val resultList = mutableListOf<CalculatorResponse>()
//            var startingNum = request.number2
//            for (i in 1..10) {
//                val result = startingNum + diff
//                resultList.add(
//                    CalculatorUnaryResponse.newBuilder()
//                        .setResult(result)
//                        .build()
//                )
//                startingNum = result
//            }
//            logger.info("ServerStreamingProducer: returning flow results")
//            return resultList.asFlow()
//        }

        // server is slow in generating sequence
        override fun deriveSubsequent(request: CalculatorRequest): Flow<CalculatorResponse> = flow {
            logger.info("ServerStreamingProducer: incoming request (${request.number1}, ${request.number2})")
            val diff = request.number2 - request.number1
            var startingNum = request.number2
            for (i in 1..10) {
                val result = startingNum + diff
                emit(CalculatorResponse.newBuilder()
                    .setResult(result)
                    .build())
                startingNum = result
                delay(1000)
                logger.info("ServerStreamingProducer: emitted next sequence $result")
            }
            logger.info("ServerStreamingCalculatorService: no more sequence to emit")
        }
    }
}

fun main() {
    val port = System.getenv("PORT")?.toInt() ?: 50051
    val server = ServerStreamingProducer(port)
    server.start()
    server.blockUntilShutdown()
}