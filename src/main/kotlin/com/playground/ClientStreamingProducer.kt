package com.playground

import com.playground.generated.CalculatorGrpcKt
import com.playground.generated.CalculatorResponse
import com.playground.generated.CalculatorRequest
import io.grpc.Server
import io.grpc.ServerBuilder
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import mu.KotlinLogging

class ClientStreamingProducer(private val port: Int) {
    private val logger = KotlinLogging.logger {}

    val server: Server = ServerBuilder
        .forPort(port)
        .addService(ClientStreamingCalculatorService())
        .build()

    fun start() {
        server.start()
        logger.info("Server started, listening on $port")
        Runtime.getRuntime().addShutdownHook(
            Thread {
                logger.info("*** shutting down gRPC server since JVM is shutting down")
                this@ClientStreamingProducer.stop()
                logger.info("*** server shut down")
            }
        )
    }

    private fun stop() {
        server.shutdown()
    }

    fun blockUntilShutdown() {
        server.awaitTermination()
    }

    private class ClientStreamingCalculatorService : CalculatorGrpcKt.CalculatorCoroutineImplBase() {
        private val logger = KotlinLogging.logger {}

        override suspend fun getMaxPair(requests: Flow<CalculatorRequest>): CalculatorResponse {
            var maxPairResult = -1
            requests.collect { request ->
                logger.info("ClientStreamingProducer: incoming request (${request.number1}, ${request.number2})")
                maxPairResult = maxOf(maxPairResult, request.number1 + request.number2)
            }
            logger.info("ClientStreamingProducer: no more incoming request.. so return result $maxPairResult now")
            return CalculatorResponse.newBuilder()
                .setResult(maxPairResult)
                .build()
        }
    }
}

fun main() {
    val port = System.getenv("PORT")?.toInt() ?: 50051
    val server = ClientStreamingProducer(port)
    server.start()
    server.blockUntilShutdown()
}