package com.playground

import com.playground.generated.CalculatorGrpcKt
import com.playground.generated.CalculatorResponse
import com.playground.generated.CalculatorRequest
import io.grpc.Server
import io.grpc.ServerBuilder
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.delay
import mu.KotlinLogging

class BidiProducer(private val port: Int) {
    private val logger = KotlinLogging.logger {}

    val server: Server = ServerBuilder
        .forPort(port)
        .addService(BidiCalculatorService())
        .build()

    fun start() {
        server.start()
        logger.info("Server started, listening on $port")
        Runtime.getRuntime().addShutdownHook(
            Thread {
                logger.info("*** shutting down gRPC server since JVM is shutting down")
                this@BidiProducer.stop()
                logger.info("*** server shut down")
            }
        )
    }

    private fun stop() {
        server.shutdown()
    }

    fun blockUntilShutdown() {
        server.awaitTermination()
    }

    private class BidiCalculatorService : CalculatorGrpcKt.CalculatorCoroutineImplBase() {
        private val logger = KotlinLogging.logger {}
        var totalSum = 0

        override fun getContinuousSum(requests: Flow<CalculatorRequest>): Flow<CalculatorResponse> = flow {
            requests.collect { request ->
                logger.info("BidiProducer: incoming pair (${request.number1}, ${request.number2})")
                totalSum += (request.number1 + request.number2)
                emit(CalculatorResponse.newBuilder()
                    .setResult(totalSum)
                    .build())
                delay(1000)
                logger.info("BidiProducer: emitted total $totalSum")
            }
        }
    }
}

fun main() {
    val port = System.getenv("PORT")?.toInt() ?: 50051
    val server = BidiProducer(port)
    server.start()
    server.blockUntilShutdown()
}