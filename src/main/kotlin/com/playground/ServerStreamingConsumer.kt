package com.playground

import com.playground.generated.CalculatorGrpcKt
import com.playground.generated.CalculatorRequest
import com.playground.generated.CalculatorResponse
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import java.io.Closeable
import java.util.concurrent.TimeUnit
import kotlinx.coroutines.flow.collect
import mu.KotlinLogging

class ServerStreamingConsumer(private val channel: ManagedChannel) : Closeable {
    private val logger = KotlinLogging.logger {}
    private val stub: CalculatorGrpcKt.CalculatorCoroutineStub = CalculatorGrpcKt.CalculatorCoroutineStub(channel)

    suspend fun deriveSubsequent(num1: Int, num2: Int) {
        val request = CalculatorRequest
            .newBuilder()
            .setNumber1(num1)
            .setNumber2(num2)
            .build()
        stub.deriveSubsequent(request).collect { streamResponse: CalculatorResponse ->
            logger.info("ServerStreamingConsumer: sequence ${streamResponse.result}")
        }
        logger.info("ServerStreamingConsumer: sequence ended")
    }

    override fun close() {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS)
    }
}

suspend fun main() {
    val port = 50051
    val channel = ManagedChannelBuilder.forAddress("localhost", port).usePlaintext().build()
    val client = ServerStreamingConsumer(channel)
    client.deriveSubsequent(1, 5)
}