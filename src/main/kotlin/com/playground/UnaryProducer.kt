package com.playground

import com.playground.generated.CalculatorGrpcKt
import com.playground.generated.CalculatorResponse
import com.playground.generated.CalculatorRequest
import io.grpc.Server
import io.grpc.ServerBuilder
import mu.KotlinLogging

class UnaryProducer(private val port: Int) {
    private val logger = KotlinLogging.logger {}
    val server: Server = ServerBuilder
        .forPort(port)
        .addService(UnaryCalculatorService())
        .build()

    fun start() {
        server.start()
        logger.info("Server started, listening on $port")
        Runtime.getRuntime().addShutdownHook(
            Thread {
                logger.info("*** shutting down gRPC server since JVM is shutting down")
                this@UnaryProducer.stop()
                logger.info("*** server shut down")
            }
        )
    }

    private fun stop() {
        server.shutdown()
    }

    fun blockUntilShutdown() {
        server.awaitTermination()
    }

    private class UnaryCalculatorService : CalculatorGrpcKt.CalculatorCoroutineImplBase() {
        private val logger = KotlinLogging.logger {}
        override suspend fun sumOnce(request: CalculatorRequest): CalculatorResponse {
            logger.info("UnaryProducer: incoming request (${request.number1}, ${request.number2})")
            val result = request.number1 + request.number2
            logger.info("UnaryProducer: returning $result")
            return CalculatorResponse.newBuilder()
                .setResult(result)
                .build()
        }
    }
}

fun main() {
    val port = System.getenv("PORT")?.toInt() ?: 50051
    val server = UnaryProducer(port)
    server.start()
    server.blockUntilShutdown()
}