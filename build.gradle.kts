import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.google.protobuf.gradle.*

plugins {
    kotlin("jvm") version "1.5.10"
    id("com.google.protobuf") version "0.8.13"
    idea // Note: need this plugin to configure idea sourceDirs
}

// Note: so that project files can reference the auto-generated classes
idea {
    module {
        // Note: proto generated code will be located at the following directories
        sourceDirs.plusAssign(file("${projectDir}/build/generated/source/proto/main/java"))
        sourceDirs.plusAssign(file("${projectDir}/build/generated/source/proto/main/grpc"))
        sourceDirs.plusAssign(file("${projectDir}/build/generated/source/proto/main/grpckt"))
    }
}

// Note: set the path to the proto directory
sourceSets {
    main {
        proto {
            srcDir("src/main/resources/proto")
        }
    }
}

group = "com.playground"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.grpc:grpc-protobuf:1.33.1")
    implementation("io.grpc:grpc-stub:1.33.1")
    implementation("io.grpc:grpc-netty:1.33.1")
    api("com.google.protobuf:protobuf-java-util:3.13.0")
    implementation("io.grpc:grpc-all:1.33.1")
    api("io.grpc:grpc-kotlin-stub:0.2.1")
    implementation("io.grpc:protoc-gen-grpc-kotlin:0.1.5")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    implementation("com.google.protobuf:protobuf-gradle-plugin:0.8.13")
    // annotation parsing for the generated code
    compileOnly("javax.annotation:javax.annotation-api:1.3.2")
    // logging
    implementation("ch.qos.logback:logback-classic:1.2.6")
    implementation("io.github.microutils:kotlin-logging-jvm:2.1.21")
    // testing
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}

protobuf {
    protoc{
        artifact = "com.google.protobuf:protoc:3.10.1"
    }
    plugins {
        id("grpc"){
            artifact = "io.grpc:protoc-gen-grpc-java:1.33.1"
        }
        id("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:0.1.5"
        }
    }
    generateProtoTasks {
        all().forEach {
            it.plugins {
                id("grpc")
                id("grpckt")
            }
        }
    }
}